## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
$ npm run migration:run
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## 演示 optimistic + version 的方法

### 情境： 在高併發的情況下，將資料表 OCounterEntity 中的一筆資料的計數欄位做遞增

##### 演示 1: 高併發時，都拿著相同的 版本號 (或更新時間) 去更新 OCounterEntity 計數，但只有一筆 request 會成功
```
$ sh src/modules/optimistic/scripts/o-case-1.sh
```

## 演示 pessimistic_read 的方法

### 情境： insert 一筆資料其 parent 為 Jones，同時併發 delete parent Jones 的資料

##### 演示 1: 併發時，insert 執行到一半發生延遲，導致 delete 先執行，但 delete 最終會成功，並且 insert 失敗
```
$ sh src/modules/pessimistic_read/scripts/pr-case-1.sh
```

##### 演示 2: 併發時，insert 執行到一半發生延遲，導致 delete 先執行，但 delete 最終會失敗，並且 insert 成功
``` 
$ sh src/modules/pessimistic_read/scripts/pr-case-2.sh
```

## 演示 pessimistic_write 的方法

### 情境： 在高併發的情況下，將資料表 PWCounterEntity 中的一筆資料的計數欄位做遞增

##### 演示 1: 高併發時，PWCounterEntity 的計數值正常增加 10 次
```
$ sh src/modules/pessimistic_write/scripts/pw-case-1.sh
```

##### 演示 2: 高併發時，PWCounterEntity 的計數值增加異常
``` 
$ sh src/modules/pessimistic_write/scripts/pw-case-2.sh
```