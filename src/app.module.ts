import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { masterDataSourceConfig } from './core/helpers/database.helper';
import { PessimisticReadModule } from './modules/pessimistic_read/pessimistic-read.module';
import { PessimisticWriteModule } from './modules/pessimistic_write/pessimistic-write.module';
import { OptimisticModule } from './modules/optimistic/optimistic.module';

@Module({
  imports: [
    PessimisticReadModule,
    PessimisticWriteModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: () => masterDataSourceConfig,
      inject: [ConfigService],
    }),
    OptimisticModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
