import * as dotenv from 'dotenv';
import { env } from 'process';
import { DataSource } from 'typeorm';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

dotenv.config();

export const masterDataSourceConfig: MysqlConnectionOptions = {
  type: 'mysql',
  host: env.DB_HOST,
  port: +env.DB_PORT,
  username: env.DB_USERNAME,
  password: env.DB_PASSWORD,
  database: env.DB_DATABASE,
  entities: ['dist/**/*.entity{.ts,.js}'],
  migrations: ['dist/src/migrations/*{.ts,.js}'],
  logging: env.TYPEORM_LOGGING === 'true',
  synchronize: false,
  dropSchema: false,
  timezone: 'Z',
};

export const masterDataSource = new DataSource(masterDataSourceConfig);
