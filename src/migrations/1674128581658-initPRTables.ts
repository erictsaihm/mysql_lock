import { MigrationInterface, QueryRunner } from 'typeorm';

export class initPRTables1674128581658 implements MigrationInterface {
  name = 'initPRTables1674128581658';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`pr_parent_entity\` (\`id\` int NOT NULL AUTO_INCREMENT COMMENT 'ID', \`name\` varchar(255) NOT NULL COMMENT '名稱', \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `CREATE TABLE \`pr_child_entity\` (\`id\` int NOT NULL AUTO_INCREMENT COMMENT 'ID', \`name\` varchar(255) NOT NULL COMMENT '名稱', \`parentId\` int NOT NULL COMMENT 'Parent Id', \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX \`REL_ab9b67fa163f3d0a32fe64078a\` (\`parentId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`pr_child_entity\` ADD CONSTRAINT \`FK_ab9b67fa163f3d0a32fe64078ad\` FOREIGN KEY (\`parentId\`) REFERENCES \`pr_parent_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );

    await queryRunner.query(
      `
        INSERT INTO pr_parent_entity (name) VALUES ('Jones');
        `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`pr_child_entity\` DROP FOREIGN KEY \`FK_ab9b67fa163f3d0a32fe64078ad\``,
    );
    await queryRunner.query(
      `DROP INDEX \`REL_ab9b67fa163f3d0a32fe64078a\` ON \`pr_child_entity\``,
    );
    await queryRunner.query(`DROP TABLE \`pr_child_entity\``);
    await queryRunner.query(`DROP TABLE \`pr_parent_entity\``);
  }
}
