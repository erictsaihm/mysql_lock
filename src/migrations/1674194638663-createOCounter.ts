import { MigrationInterface, QueryRunner } from 'typeorm';

export class createOCounter1674194638663 implements MigrationInterface {
  name = 'createOCounter1674194638663';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`o_counter_entity\` (\`id\` int NOT NULL AUTO_INCREMENT COMMENT 'ID', \`counter\` int NOT NULL COMMENT '計數器' DEFAULT '0', \`version\` int NOT NULL COMMENT '版本號' DEFAULT '1', \`createdAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `
        INSERT INTO o_counter_entity (id, counter) VALUES (1, 1);
        `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE \`o_counter_entity\``);
  }
}
