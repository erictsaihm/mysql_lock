import { MigrationInterface, QueryRunner } from 'typeorm';

export class createPWCounter1674132643897 implements MigrationInterface {
  name = 'createPWCounter1674132643897';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`pw_counter_entity\` (\`id\` int NOT NULL AUTO_INCREMENT COMMENT 'ID', \`counter\` int NOT NULL COMMENT '計數器' DEFAULT '0', \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );

    await queryRunner.query(
      `
        INSERT INTO pw_counter_entity (id, counter) VALUES (1, 1);
        `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE \`pw_counter_entity\``);
  }
}
