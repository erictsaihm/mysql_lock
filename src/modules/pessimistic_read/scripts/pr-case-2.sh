curl --location --request POST 'http://localhost:3001/pessimistic-reads/pessimistic-read/success' \
--header 'Content-Type: application/json' \
--data-raw '{
    "parentName": "Jones",
    "childName": "Peter"
}' &
sleep 1 &&
curl --location --request DELETE 'http://localhost:3001/pessimistic-reads/pessimistic-read?parentName=Jones' &
