import { Body, Controller, Delete, Post, Query } from '@nestjs/common';
import { CreatePRChildDto } from './dto/create-pr-child.dto';
import { DeletePRParentDto } from './dto/delete-pr-parent.dto';
import { PessimisticReadService } from './pessimistic-read.service';

/** 演示 悲觀鎖 (pessimistic_read) 的使用情境 */
@Controller('/pessimistic-reads/pessimistic-read')
export class PessimisticReadController {
  constructor(
    private readonly pessimisticReadService: PessimisticReadService,
  ) {}

  @Post('success')
  async createSuccess(@Body() dto: CreatePRChildDto) {
    return this.pessimisticReadService.createSuccess(dto);
  }

  @Post('failure')
  async createFailed(@Body() dto: CreatePRChildDto) {
    return this.pessimisticReadService.createFailure(dto);
  }

  @Delete()
  async delete(@Query() dto: DeletePRParentDto) {
    return this.pessimisticReadService.delete(dto);
  }
}
