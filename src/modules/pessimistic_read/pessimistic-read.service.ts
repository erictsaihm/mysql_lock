import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { CreatePRChildDto } from './dto/create-pr-child.dto';
import { DeletePRParentDto } from './dto/delete-pr-parent.dto';
import { PRChildEntity } from './entities/pr-child.entity';
import { PRParentEntity } from './entities/pr-parent.entity';

@Injectable()
export class PessimisticReadService {
  constructor(private dataSource: DataSource) {
    return;
  }

  /** 此範例演示：
   * 在 child table 新增一個 parent 為 Jones 的 child: Peter，為了避免新增前 parent 就被刪除，因此將針對 parent table 上鎖 */
  async createSuccess(dto: CreatePRChildDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const entity = await queryRunner.manager.findOne(PRParentEntity, {
        where: { name: dto.parentName },
        lock: { mode: 'pessimistic_read' },
      });

      if (!entity) {
        throw new Error('Failed to find parent');
      }

      // 製造延時
      await new Promise((resolve) => setTimeout(resolve, 3000));

      await queryRunner.manager.insert(PRChildEntity, {
        name: dto.childName,
        parentId: entity.id,
      });

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
    }
  }

  /** 此範例演示：
   * 在 child table 新增一個 parent 為 Jones 的 child: Peter，沒有針對 parent table 上鎖，所以刪除 Parent 可能成功導致 insert child 失敗 */
  async createFailure(dto: CreatePRChildDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const entity = await queryRunner.manager.findOne(PRParentEntity, {
        where: { name: dto.parentName },
      });

      // 製造延時
      await new Promise((resolve) => setTimeout(resolve, 3000));

      await queryRunner.manager.insert(PRChildEntity, {
        name: dto.childName,
        parentId: entity.id,
      });

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
    }
  }

  async delete(dto: DeletePRParentDto) {
    return await this.dataSource.manager
      .delete(PRParentEntity, {
        name: dto.parentName,
      })
      .catch((e) => console.error(e.message));
  }
}
