import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PRChildEntity } from './entities/pr-child.entity';
import { PRParentEntity } from './entities/pr-parent.entity';
import { PessimisticReadController } from './pessimistic-read.controller';
import { PessimisticReadService } from './pessimistic-read.service';

@Module({
  imports: [TypeOrmModule.forFeature([PRParentEntity, PRChildEntity])],
  controllers: [PessimisticReadController],
  providers: [PessimisticReadService],
})
export class PessimisticReadModule {}
