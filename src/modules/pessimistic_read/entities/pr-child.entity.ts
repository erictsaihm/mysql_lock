import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PRParentEntity } from './pr-parent.entity';

@Entity()
export class PRChildEntity {
  @PrimaryGeneratedColumn({
    comment: 'ID',
  })
  id: number;

  @Column({
    type: 'varchar',
    comment: '名稱',
  })
  name: string;

  @OneToOne(() => PRParentEntity)
  @JoinColumn()
  parent: PRParentEntity;

  @Column({
    type: 'int',
    comment: 'Parent Id',
  })
  parentId: number;

  @CreateDateColumn({
    type: 'datetime',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'datetime',
  })
  updatedAt: Date;
}
