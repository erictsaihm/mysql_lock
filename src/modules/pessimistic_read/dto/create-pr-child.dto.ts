import { IsNotEmpty, IsString } from 'class-validator';

export class CreatePRChildDto {
  @IsNotEmpty()
  @IsString()
  public readonly childName: string;

  @IsNotEmpty()
  @IsString()
  public readonly parentName: string;
}
