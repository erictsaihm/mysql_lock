import { IsNotEmpty, IsString } from 'class-validator';

export class DeletePRParentDto {
  @IsNotEmpty()
  @IsString()
  public readonly parentName: string;
}
