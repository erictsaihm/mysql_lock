import { IsDateString, IsInt, IsNotEmpty, IsOptional } from 'class-validator';

export class CreateODto {
  @IsNotEmpty()
  @IsInt()
  public readonly id: number;

  @IsOptional()
  @IsInt()
  public readonly version: number;

  @IsOptional()
  @IsDateString()
  public readonly updatedAt: string;
}
