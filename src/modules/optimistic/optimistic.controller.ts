import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { CreateODto } from './dto/create-o.dto';
import { OptimisticService } from './optimistic.service';

@Controller('optimistics/optimistic')
export class OptimisticController {
  constructor(private readonly optimisticService: OptimisticService) {}

  @Get()
  async findOne(@Query() dto: CreateODto) {
    return await this.optimisticService.findOne(dto);
  }

  @Post('success')
  async createSuccess(@Body() dto: CreateODto) {
    return await this.optimisticService.createSuccess(dto);
  }
}
