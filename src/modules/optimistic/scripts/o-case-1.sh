UPDATED="2023-01-20T06:06:05.000Z"
VERSION=2
for i in {1..10}; do 
  curl --location --request POST 'http://localhost:3001/optimistics/optimistic/success' \
  --header 'Content-Type: application/json' \
  --data-raw "{ \"id\": 1, \"version\": $VERSION, \"updatedAt\": \"$UPDATED\" }" &
done