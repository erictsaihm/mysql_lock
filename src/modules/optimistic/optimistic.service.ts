import { Injectable } from '@nestjs/common';
import dayjs from 'dayjs';
import { DataSource } from 'typeorm';
import { CreateODto } from './dto/create-o.dto';
import { OCounterEntity } from './entities/o-counter.entity';

@Injectable()
export class OptimisticService {
  constructor(private dataSource: DataSource) {
    return;
  }

  async findOne(dto: CreateODto) {
    return await this.dataSource
      .createQueryRunner()
      .manager.findOne(OCounterEntity, {
        where: { id: dto.id },
      });
  }

  /** 此範例演示：
   *  根據傳入的參數版本號 (或更新時間) 來查找資料並更新欄位，併發時將以最新的版本號 (或更新時間) 才能成功更新資料 */
  async createSuccess(dto: CreateODto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const entity = await queryRunner.manager.findOne(OCounterEntity, {
        where: { id: dto.id },
        lock: {
          mode: 'optimistic',
          // version: dto.version, // 也可改用版本號來當樂觀鎖
          version: dayjs(dto.updatedAt).toDate(),
        },
      });

      const result = await queryRunner.manager.update(
        OCounterEntity,
        {
          id: entity.id,
          // version: dto.version, // 也可改用版本號來當樂觀鎖
          updatedAt: dayjs(dto.updatedAt).toDate(),
        },
        {
          counter: entity.counter + 1,
        },
      );

      // 檢查是否更新成功
      if (result.affected <= 0) {
        throw new Error('Failed to update counter');
      }

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
    }

    return await this.dataSource.manager.findOne(OCounterEntity, {
      where: { id: dto.id },
    });
  }
}
