import { Test, TestingModule } from '@nestjs/testing';
import { OptimisticController } from './optimistic.controller';

describe('OptimisticController', () => {
  let controller: OptimisticController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OptimisticController],
    }).compile();

    controller = module.get<OptimisticController>(OptimisticController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
