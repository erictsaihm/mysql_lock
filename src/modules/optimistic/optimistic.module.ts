import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OCounterEntity } from './entities/o-counter.entity';
import { OptimisticController } from './optimistic.controller';
import { OptimisticService } from './optimistic.service';

@Module({
  imports: [TypeOrmModule.forFeature([OCounterEntity])],
  controllers: [OptimisticController],
  providers: [OptimisticService],
})
export class OptimisticModule {}
