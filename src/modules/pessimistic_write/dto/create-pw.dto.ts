import { IsInt, IsNotEmpty } from 'class-validator';

export class CreatePWDto {
  @IsNotEmpty()
  @IsInt()
  public readonly id: number;
}
