import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { CreatePWDto } from './dto/create-pw.dto';
import { PWCounterEntity } from './entities/pw-counter.entity';

@Injectable()
export class PessimisticWriteService {
  constructor(private dataSource: DataSource) {
    return;
  }

  /** 此範例演示：
   * 將對 PWCounterEntity 中的計數欄位遞增值，但併發情況下遞增值會有髒讀 (dirty read) 的情況，所以需要上讀寫鎖後才做遞增計數 */
  async createSuccess(dto: CreatePWDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const entity = await queryRunner.manager.findOne(PWCounterEntity, {
        where: { id: dto.id },
        lock: { mode: 'pessimistic_write' },
      });

      if (!entity) {
        throw new Error('Failed to find counter');
      }

      await queryRunner.manager.update(
        PWCounterEntity,
        {
          id: entity.id,
        },
        {
          counter: entity.counter + 1,
        },
      );

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
    }

    return await this.dataSource.manager.findOne(PWCounterEntity, {
      where: { id: dto.id },
    });
  }

  /** 此範例演示：
   * 將對 PWCounterEntity 中的計數欄位遞增值，由於沒有上鎖，所以高併發情況下，髒讀 (dirty read) 將導致計數異常 */
  async createFailure(dto: CreatePWDto) {
    const entity = await this.dataSource.manager.findOne(PWCounterEntity, {
      where: { id: dto.id },
    });

    await this.dataSource.manager.update(
      PWCounterEntity,
      {
        id: entity.id,
      },
      {
        counter: entity.counter + 1,
      },
    );

    return await this.dataSource.manager.findOne(PWCounterEntity, {
      where: { id: dto.id },
    });
  }
}
