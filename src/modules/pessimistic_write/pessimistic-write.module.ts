import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PWCounterEntity } from './entities/pw-counter.entity';
import { PessimisticWriteController } from './pessimistic-write.controller';
import { PessimisticWriteService } from './pessimistic-write.service';

@Module({
  imports: [TypeOrmModule.forFeature([PWCounterEntity])],
  controllers: [PessimisticWriteController],
  providers: [PessimisticWriteService],
})
export class PessimisticWriteModule {}
