import { Body, Controller, Post } from '@nestjs/common';
import { CreatePWDto } from './dto/create-pw.dto';
import { PessimisticWriteService } from './pessimistic-write.service';

/** 演示 悲觀鎖 (pessimistic_write) 的使用情境 */
@Controller('/pessimistic-writes/pessimistic-write')
export class PessimisticWriteController {
  constructor(
    private readonly pessimisticWriteService: PessimisticWriteService,
  ) {}

  @Post('success')
  async createSuccess(@Body() dto: CreatePWDto) {
    return this.pessimisticWriteService.createSuccess(dto);
  }

  @Post('failure')
  async createFailed(@Body() dto: CreatePWDto) {
    return this.pessimisticWriteService.createFailure(dto);
  }
}
